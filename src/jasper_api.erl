%% @author samuel
%% @doc follow this: http://<host>:<port>/jasperserver[-pro]/rest_v2/reports/path/to/report.<format>?<arguments> 
%% @doc	jasper_api:pull_jasper_report("http://127.0.0.1:8080","Reports", "jasperadmin", "jasperadmin", "report", "pdf", [{id, 9491}], []).


-module(jasper_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([pull_jasper_report/8]).



%% ====================================================================
%% Internal functions
%% ====================================================================


pull_jasper_report(HostPort,PathToReport, Username, Password, ReportName, ReportFormat, Params, SaveDir) -> 

	Timeout = 50000,
	ContentType = "text/json",
    Headers = [auth_header(Username, Password), {"Content-Type",ContentType}],
    Options = [{body_format,binary}],
	
	URL = HostPort++"/jasperserver/rest_v2/reports/"++PathToReport++ReportName++"."++ReportFormat++get_params(Params, 0, ""),

    Response = httpc:request(get, {URL, Headers}, [{timeout, Timeout}],Options),

    case Response of
		
        {ok, {{"HTTP/1.1",200,_}, _HTTPHeaders,BinResponseData}} ->
				case SaveDir of
					[]->
						StrDir = ReportName++"."++ReportFormat,
						file:write_file(StrDir, BinResponseData);
					_->
						StrDir = SaveDir++"/"++ReportName++"."++ReportFormat,
						file:write_file(SaveDir++"/"++ReportName++"."++ReportFormat, BinResponseData)
				end;
        Error ->
			Error
    end.

get_params([], _N, Param)->
	Param;
get_params([{Key, Value}| Rest], N, Param)when N == 0->
	NewParam = Param++"?"++to_list(Key)++"="++to_list(Value),
	get_params(Rest, N+1, NewParam);
get_params([{Key, Value}| Rest], N, Param)when N > 0->
	NewParam = Param++"&"++to_list(Key)++"="++to_list(Value),
	get_params(Rest, N+1, NewParam).

auth_header(User, Pass) ->
	Encoded = base64:encode_to_string(lists:append([User,":",Pass])),
	{"Authorization","Basic " ++ Encoded}.

to_list(<<"'NULL'">>) ->
	"";
to_list(<<"NULL">>) ->
	"";
to_list(undefined) ->
	"";
to_list(Int) when is_integer(Int) -> 
	integer_to_list(Int);
to_list(Bin) when is_binary(Bin) ->
	binary_to_list(Bin);
to_list(Atom) when is_atom(Atom) ->
	atom_to_list(Atom);
to_list(List ) ->
	List.