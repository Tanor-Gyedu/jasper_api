%%%-------------------------------------------------------------------
%% @doc jasper_api public API
%% @end
%%%-------------------------------------------------------------------

-module(jasper_api_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    jasper_api_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
